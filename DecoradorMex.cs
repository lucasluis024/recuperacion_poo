﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace examen_re

{
    class DecoradorMex : OrdenBase
    {
        protected OrdenBase orden;

        public DecoradorMex(OrdenBase orden)
        {
            this.orden = orden;
        }
        public virtual double CalcularTotalPrecio()
        {
            Console.WriteLine("Precio Calculado desde el decorador");
            return orden.CalcularTotalPrecio();
        }
    }
}